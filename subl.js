#!/usr/bin/env node

// Imports
const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

// Config
const storeFolder = path.join(process.env.HOME, '.config/sublime-projects');
const sublimePath = '/opt/sublime_text/sublime_text'

let currentDir = process.cwd();
let dirName = path.basename(currentDir);
let projectName = dirName.toLowerCase().replace(/\s/g, '_');
let projectFile = path.join(storeFolder, projectName + '.sublime-project');

let sublimeCommand = `${sublimePath} --project ${projectFile}`

// Create project file if it does not exist
if (!fs.existsSync(projectFile)) {
	fs.writeFileSync(projectFile, getProjectContent(currentDir), 'utf8');	
}

// Run sublime
exec(sublimeCommand);


function getProjectContent(path) {
	return `
{
    "folders":
    [
        {
            "path": "${path}"
        }
    ]
}
	`;
}